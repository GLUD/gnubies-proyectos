# 🎓 GNUBIES PROYECTOS

Este repositorio se realiza con el fin de recopilar todos los proyectos desarrollados durante las clases de gnubies dadas en el periodo 2019-III y 2021-III, Se recopilan los proyectos realizados por los grupos de 👻 @juferoga, 🤓 @edaagapu, 💼 @jarcoz, 🐱 @LeidyAldana, 🦊 @andabed y 😃 @seluroor

## Indice

### 2019-III

1) [Libro Glud](https://gitlab.com/edaagapu/libroglud.git "Repositorio Libro") 📓
2) [Proyecto Precipitación](https://gitlab.com/Juferoga/precipitacion-project.git "Repositorio proyecto") ☔
3) [Proyecto Manual de usuario Openshot](https://gitlab.com/Juferoga/manualusuarioopenshot.git "Repositorio Manual de usuario") 🎬

### 2021-I

1) [Linux Gamnig](https://gitlab.com/jamalambom/linux_gaming) 🎮
2) [Flujo Multifásico](https://github.com/SSA1917/Flujo_Multifasico) 💧 
3) [Bash Customaizer](https://github.com/MoraDiego/Bash-Customizer) 💻
4) [GeoRecordatorio](https://gitlab.com/LunaGaitan/georecodatorio/-/tree/master/Georecordatorio) 🌎
5) [Free Software Song Cover](https://www.youtube.com/watch?v=4vrKtX6jFrg) 🎵
6) [Guia de personalización de I3](https://github.com/MikeGarzon/Guia_de_personalizacion) 💻
7) [Experimentación músical a partir de una ecuación](https://github.com/k-delta/GNU/tree/master) 📈 🎵
8) [Flotador](https://github.com/SebSalazar/flotador-gnu-linux-final) ✅
9) [Transmisión de pantalla a un dispositivo movil por medio de red wifi](https://github.com/CarlosGuerrero9909/Proyecto-Gnubies) 📱
10) [Distribución GnubieX](https://gitlab.com/mafuentesr/proyecto-final-gnubies/-/tree/main/) 💯
11) [SunCalc](https://github.com/Rochinchi/SunCalc) ☀️
12) [Música libre](https://gitlab.com/ydacostab/proyecto-software-libre) 🎸

### Gnubies - GLUD
